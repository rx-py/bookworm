#!/bin/bash

# Check for updates on Debian
apt_updates=$(apt list --upgradable 2>/dev/null | grep -c upgradable)

# APT = 

if [ "$apt_updates" -gt 0 ]; then
    echo " $apt_updates"
else
    echo "  0"
fi

