#!/bin/bash

# Function to get current brightness percentage
get_brightness_percentage() {
    local max_brightness=$(cat /sys/class/backlight/*/max_brightness)
    local current_brightness=$(cat /sys/class/backlight/*/brightness)
    local brightness_percentage=$((current_brightness * 100 / max_brightness))
    echo "$brightness_percentage%"
}

# Call the function and output the result
get_brightness_percentage

